<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lista de Paises</title>
</head>
<body>
    <h1>Lista de Paises</h1>
    <table>
        <thead>
            <tr>
                <th>Pais</th>
            </tr>
        </thead>
        <tbody>
            @foreach($paises as $pais => $infopais)
                <tr>
                    <td>
                    {{ $pais }}
                    </td>
                    <td>
                    {{ $infopais ["Capital"] }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>