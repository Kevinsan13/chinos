<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//PRIMERA RUTA DE GET
Route::get('/', function () {
    return view('welcome');
});
Route::get('hola', function(){
echo "Hola, estoy en laravel";
}
);

Route::get('arreglo', function () {
//CREAR UN ARREGLO DE ESTUDIANTES
	$estudiantes = ["AD" => "Andres",
	"LR" => "Laura",
	"ANN" => "Ana",
	"RDR" => "Rodrigo"];
//var_dump($estudiantes);

//RECORRER UN ARREGLO
	foreach ($estudiantes as $indice => $e ) {
		echo "$e tiene indice $indice <hr />";
	}
});
Route :: get('paises', function(){

	//Crear un arreglo con informacion de paises
	$paises = [
		"Colombia" => [
						"Capital" => "Bogota",
						"Moneda" => "Pesos",
						"Poblacion" => "50.372 Millones"
					  ],
		"Ecuador" => [
						"Capital" => "Quito",
						"Moneda" => "Dolar",
						"Poblacion" => "17.517 Millones" 
					 ],
		"Brasil" => [
						"Capital" => "Brasilia",
						"Moneda" => "Real",
						"Poblacion" => "212.216  Millones"
					],
		"Bolivia" => [
						"Capital" => "La Paz",
						"Moneda" => "Boliviano",
						"Poblacion" => "11.633 Millones"
					 ]
	];
//Recorrer la primera dimension del arreglo
//foreach ( $paises as $pais => $infopais){
//	echo "<h2> $pais </h2>";
//	echo "Capital:", $infopais["Capital"],"<br />";
//	echo "Moneda:", $infopais["Moneda"],"<br />";
//	echo "Población:", $infopais["Poblacion"],"<br />";
//}

//Mostrar una vista para presentar los paises
//En MVC se puede pasar datos a una vista
return view ('paises')->with('paises' , $paises);
});
